<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmergencyMail extends Mailable
{
    use Queueable, SerializesModels;
    public $emergencyData;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $emergencyData)
    {
        $this->emergencyData =  $emergencyData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('Email.emergencyEmail')
                    ->with('emergencyData', $this->emergencyData);
    }
}
