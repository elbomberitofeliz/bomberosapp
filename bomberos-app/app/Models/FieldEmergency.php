<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FieldEmergency extends Model
{
    protected $table = 'metadata_emergency_field';

    protected $fillable = [
        'name',
    ];
}
