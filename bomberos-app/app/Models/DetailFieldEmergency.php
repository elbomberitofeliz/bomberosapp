<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailFieldEmergency extends Model
{
    protected $table = 'metadata_emergency_detail';

    protected $fillable = [
        'emergency_id',
        'field_id',
        'value'
    ];
}
