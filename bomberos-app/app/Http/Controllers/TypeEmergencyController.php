<?php

namespace App\Http\Controllers;

use App\Models\TypeEmergency;
use Illuminate\Http\Request;

class TypeEmergencyController extends Controller
{
    public function get($type){
        $typesEmergency = TypeEmergency::where('category_id',$type)->get();

        return $typesEmergency;
    }
}
