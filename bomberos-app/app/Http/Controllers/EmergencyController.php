<?php

namespace App\Http\Controllers;

use App\Mail\EmergencyMail;
use App\Models\DetailFieldEmergency;
use App\Models\Emergency;
use App\Models\FieldEmergency;
use App\Models\TypeCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpFoundation\Response;
class EmergencyController extends Controller
{
    public function save(Request $request){

        $category = $request->Category;
        $situation = $request->Situation;
        $Respondent_id = $request->Respondent_id;
        $Description = $request->Description;
        $Direction = $request->Direction;

        $name = $category.$situation.$Respondent_id.$Description.$Direction;

        $emergency = Emergency::where('name',$name)->first();
        
        if($emergency == null){
            $newEmergency = new Emergency();
            $newEmergency->name = $name;
            $newEmergency->save();

            $fields = FieldEmergency::get();

            foreach ($fields as $field) {
                $fieldName = $field->name;
                $value = $request->$fieldName;

                if($value != null){
                    $detailCategory = new DetailFieldEmergency();
                    $detailCategory->value = $value;
                    $detailCategory->emergency_id = $newEmergency->id;
                    $detailCategory->field_id = $field->id;       
                    $detailCategory->save();
                }

                $newEmergency->$fieldName = $value;
            }

            $email = ['607-JKguv8w8WHh6zaNX@esalert.active911.com','jeisonortiz0016@gmail.com'];
            Mail::to($email)->send(new EmergencyMail($newEmergency));
       
            return response()->json([
                'Emergencia enviada new' => True,
            ], Response::HTTP_OK);

        }else{
            return response()->json([
                'Emergencia enviada' => True,
            ]);
        }
    }

    public function save2(Request $request){

        $typeCategory = TypeCategory::find($request->typeCategory);
        $category = $typeCategory->name; 
        $situation = $request->typeEmergency;
        $respondent_id = auth()->user()->name;
        $description = $request->description;
        $direction = $request->address;
        $Neighbor = $request->barrio;

        $Risk = 'Leve';
        $grupo = 'Pruebas';

        
        $name = $category.$situation.$respondent_id.$description.$direction;

        $emergency = Emergency::where('name',$name)->first();
        
        if($emergency == null){
            $newEmergency = new Emergency();
            $newEmergency->name = $name;
            $newEmergency->save();

            for ($i=0; $i < 15; $i++) { 
                $detailCategory = new DetailFieldEmergency();
                $detailCategory->emergency_id = $newEmergency->id;

                switch ($i) {
                    case 0:
                        $detailCategory->field_id = 1;
                        $detailCategory->value = $category;
                        $newEmergency->Category = $category;
                        break;
                    case 1:
                        $detailCategory->field_id = 2;
                        $detailCategory->value = $situation;
                        $newEmergency->Situation = $situation;
                        break;
                    case 2:
                        $detailCategory->field_id = 3;
                        $detailCategory->value = $Neighbor;
                        $newEmergency->Neighbor = $Neighbor;
                        break;
                    case 3:
                        $detailCategory->field_id = 4;
                        $detailCategory->value = $description;
                        $newEmergency->Description = $description;
                        break;
                    case 4:
                        $detailCategory->field_id = 5;
                        $detailCategory->value = $direction;
                        $newEmergency->Direction = $direction;
                        break;
                    case 5:
                        $detailCategory->field_id = 6;
                        $detailCategory->value = $respondent_id;
                        $newEmergency->Respondent_id  = $respondent_id;
                        break;
                    case 6:
                        $detailCategory->field_id = 7;
                        $detailCategory->value = $Risk;
                        $newEmergency->Risk = $Risk;
                        break;
                    case 7:
                        $detailCategory->field_id = 8;
                        $detailCategory->value = $grupo;
                        $newEmergency->Grupo = $grupo;
                        break;
                    case 8:
                        $detailCategory->field_id = 9;
                        $detailCategory->value = $request->address_gps;
                        $newEmergency->Emergency_direction = $request->address_gps;
                        break;
                    case 9:
                        $detailCategory->field_id = 10;
                        $detailCategory->value = $request->lat;
                        $newEmergency->Emergency_lat  = $request->lat;
                        break;
                    case 10:
                        $detailCategory->field_id = 11;
                        $detailCategory->value = $request->lng;
                        $newEmergency->Emergency_lng  = $request->lng;
                        break;
                    case 11:
                        $detailCategory->field_id = 12;
                        $detailCategory->value = $request->address_gps;
                        $newEmergency->Report_direction = $request->address_gps;
                        break;
                    case 12:
                        $detailCategory->field_id = 13;
                        $detailCategory->value = $request->lat;
                        $newEmergency->Report_lat = $request->lat;
                        break;
                    case 13:
                        $detailCategory->field_id = 14;
                        $detailCategory->value = $request->lng;
                        $newEmergency->Report_lng = $request->lng;
                        break;
                    case 14:
                        $detailCategory->field_id = 15;
                        $detailCategory->value = $request->numerocontacto;
                        $newEmergency->Number_phone = $request->numerocontacto;
                        break;   
                    default:
                        # code...
                        break;
                }
                
                $detailCategory->save();
            }
        
            $email = ['607-JKguv8w8WHh6zaNX@esalert.active911.com','jeisonortiz0016@gmail.com'];
            Mail::to($email)->send(new EmergencyMail($newEmergency));
       
            return back();

        }else{
            return back();
        }
    }

}
