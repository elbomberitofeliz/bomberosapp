<?php

namespace App\Http\Controllers;

use App\Models\TypeCategory;
use App\Models\TypeEmergency;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $typeCategory = TypeCategory::get();
        $typeEmergency = TypeEmergency::get();

        return view('home')->with(compact('typeCategory','typeEmergency'));
    }
}
