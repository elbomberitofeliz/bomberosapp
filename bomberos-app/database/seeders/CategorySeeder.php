<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_category')->insert([
            ['name' => 'INCENDIO'],
            ['name' => 'MATERIALES PELIGROSOS'],
            ['name' => 'ACCIDENTES'],
            ['name' => 'RESCATE'],
            ['name' => 'NATURALES'],
            ['name' => 'PREVENCION'],
        ]);
    }
}
