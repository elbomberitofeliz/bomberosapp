<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FieldSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('metadata_emergency_field')->insert([

                ['name' => 'Category'],
                ['name' => 'Situation'],
                ['name' => 'Neighbor'],
                ['name' => 'Description'],
                ['name' => 'Direction'],
                ['name' => 'Respondent_id'],
                ['name' => 'Risk'],
                ['name' => 'Grupo'],
                ['name' => 'Emergency_direction'],
                ['name' => 'Emergency_lat'],
                ['name' => 'Emergency_lng'],
                ['name' => 'Report_direction'],
                ['name' => 'Report_lat'],
                ['name' => 'Report_lng'],
                ['name' => 'Number_phone'],
        ]);
    }
}
