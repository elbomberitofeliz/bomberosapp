<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;



class TypeEmergencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_emergency')->insert([
            [
                'name' => 'INDUSTRIAL',
                'category_id' => 1
            ],
            [
                'name' => 'FORESTAL',
                'category_id' => 1
            ],
            [
                'name' => 'VEHICULAR',
                'category_id' => 1
            ],
            [
                'name' => 'ESTRUCTURAL',
                'category_id' => 1
            ],
            [
                'name' => 'DE PASTOS',
                'category_id' => 1
            ],
            [
                'name' => 'DE BASURAS',
                'category_id' => 1
            ],
            [
                'name' => 'ELECTRICO',
                'category_id' => 1
            ],
            [
                'name' => 'PRUEBAS',
                'category_id' => 6
            ],
        ]);
    }
}
