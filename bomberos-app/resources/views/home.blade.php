@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reportar Emergencia') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="row g-3" action="{{ route('emergency.store2') }}" method="post">
                        @csrf
                        <div class="col-md-6">
                          <label for="typeCategory" class="form-label">Categoria de emergencia</label>
                          <select class="form-select form-control" id="typeCategory" name="typeCategory" data-placeholder="Choose one thing">
                            @foreach($typeCategory as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach                   
                        </select>
                        </div>
                        <div class="col-md-6">
                            <label for="typeEmergency" class="form-label">Tipo de emergencia</label>
                            <select class="form-select form-control" id="typeEmergency" name="typeEmergency" data-placeholder="Choose one thing">
                            @foreach($typeEmergency as $emergency)
                                <option value="{{ $emergency->name }}">{{ $emergency->name }}</option>
                            @endforeach                      
                          </select>
                        </div>
                        <div class="col-12">
                          <label for="barrio" class="form-label">Barrio</label>
                          <input type="text" class="form-control" id="barrio" name="barrio" placeholder="">
                        </div>
                        <div class="col-12">
                          <label for="numerocontacto" class="form-label">Número de contacto</label>
                          <input type="text" class="form-control" id="numerocontacto" name="numerocontacto" placeholder="">
                        </div>
                        <div class="col-12">
                          <label for="address" class="form-label">Dirección</label>
                          <input type="text" class="form-control" id="address" name="address" placeholder="">
                        </div>
                        <div class="col-12">
                          <label for="description" class="form-label">Descripción de lo que sucede</label>
                          <textarea type="areatext" class="form-control" id="description" name="description" placeholder=""></textarea>
                        </div>

                        <div class="row g-3">
                          <b><label for="typeCategory" class="form-label">Coordenadas de la emergencia</label></b>
                          <div class="col-md-6">
                            <label for="lat" class="form-label">Lat</label>
                            <input type="text" class="form-control" id="lat" name="lat" placeholder="" readonly>
                          </div>
                          <div class="col-md-6">
                              <label for="lng" class="form-label">Lng</label>
                              <input type="text" class="form-control" id="lng" name="lng" placeholder="" readonly>
                          </div>

                          <div class="col-12">
                            <label for="address_gps" class="form-label">Dirección GPS</label>
                            <input type="text" class="form-control" id="address_gps" name="address_gps" placeholder="" readonly>
                          </div>
                        </div>

                        <div id="map"></div>
                        <div class="col-12">
                          <button type="submit" class="btn btn-primary">Enviar alerta</button>
                        </div>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
  var map = L.map('map').
     setView([4.733864797051263, -74.26445603370668],
     15);

     L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>',
    maxZoom: 18
    }).addTo(map);

    
    var layerGroup = L.layerGroup().addTo(map);

    map.on('click', function(e){

      var coord = e.latlng;
      var lat = coord.lat;
      var lng = coord.lng;

      $.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+ lat + ','+lng+'&key=AIzaSyD4mfdkdf4jvc6SXpIIo6p1XcW9vsO5Kp0', function(data){
        document.getElementById("address_gps").value = data.results[0].formatted_address;
      });
      layerGroup.clearLayers();
      L.marker([lat, lng], "text").addTo(layerGroup);

      document.getElementById("lat").value = lat;
      document.getElementById("lng").value = lng;
     
    });

    const typeEmergency = document.getElementById('typeEmergency');
    const typeCategory = document.getElementById('typeCategory');
    

    const opcionCambiada = () => {
      limpiar();
      cambio();
    };

    typeCategory.addEventListener("change", opcionCambiada);

    const limpiar = () => {
      for (let i = typeEmergency.options.length; i >= 0; i--) {
        typeEmergency.remove(i);
      }
    };

    const cambio = () => {
      $.get(`https://bomberosmadridb31.com/web_service/bomberos-app/public/typeEmergency/get/`+typeCategory.value , function(data){

        data.forEach(emergency => {
          const option = document.createElement('option');
          option.value = emergency.name;
          option.text = emergency.name;
          typeEmergency.appendChild(option);
        });
        
      });
    }
</script>
@endsection
