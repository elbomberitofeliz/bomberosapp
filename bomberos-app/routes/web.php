<?php
use App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use PhpParser\Node\Expr\FuncCall;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::namespace("App\\Http\\Controllers")->group(function () {
    Route::PUT("/save", "EmergencyController@save");
    Route::POST("/save2",[
        'uses' => "EmergencyController@save2",
        'as' => "emergency.store2"
    ]);
    Route::get("/typeEmergency/get/{type}",[
        'uses' => "TypeEmergencyController@get",
        'as' => "typeEmergency.get"
    ]);
});

Route::get('/token', function () {
    return csrf_token();
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
